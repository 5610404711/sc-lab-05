package library;

import java.util.ArrayList;

public class Book {
	private String bname;
	private String author;
	private int number;

	public Book(String bname,int number,String author){
		this.bname = bname;
		this.author = author;
		this.number = number;
	}
	
	public String getBname() {
		// TODO Auto-generated method stub
		return bname;
		
	}
		
	public String getAuthor() {
		// TODO Auto-generated method stub
		return author;
		
	}
	
	public int getNumber(){
		return number;
	}

	
	
	
}
