package library;

import java.util.ArrayList;

public class Student {
	
	private String name;
	private String lastname;
	private String id;
	private ArrayList<Book> books;
	
	public Student(String name,String lastname,String id){
		this.name = name;
		this.lastname = lastname;
		this.id = id;
		this.books = new ArrayList<Book>();
	}
	
	public String getName() {
		return name;
	}
		
	public String getLastname() {
		// TODO Auto-generated method stub
		return lastname;
		
	}
		
	public String getId() {
		// TODO Auto-generated method stub
		return id;
		
	}
			
	public String getBorrowedBook() {
		// TODO Auto-generated method stub
		String namebook = "";
		for(Book b: books){
			namebook = b.getBname();
		}
		return namebook;
		
	}
	


	public ArrayList<Book> getBooks(){

		return books;
	}
	
	public void addbook(Book b){
		books.add(b);
	}

}
	

