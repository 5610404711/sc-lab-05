package library;

import java.util.ArrayList;

public class Instructor {
	private String name;
	private String lastname;
	private String major;
	private ArrayList<Book> ibook;
	
	public Instructor(String name,String lastname,String major){
		this.name = name;
		this.lastname = lastname;
		this.major = major;
		this.ibook = new ArrayList<Book>();
	}
	
	public String getName() {
		// TODO Auto-generated method stub
		return name;
		
	}
		
	public String getLastname() {
		// TODO Auto-generated method stub
		return lastname;
		
	}
		
	public String getMajor() {
		// TODO Auto-generated method stub
		return major;
		
	}
	
	public String getBorrowedBook() {
		// TODO Auto-generated method stub
		String namebook = "";
		for(Book a: ibook){
			namebook = a.getBname();
		}
		return namebook;
		
	}
	

	public ArrayList<Book> getBooks(){

		return ibook;
	}
	
	public void addbook(Book b){
		ibook.add(b);
	}

}
