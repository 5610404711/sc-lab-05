package library;

import java.util.ArrayList;

public class Library {
	ArrayList<Book> blist;
	
	public Library(){
		blist = new ArrayList<Book>();
	}
	
	public void addBook(Book bk){
		blist.add(bk);
	}
	
	public void addRefBook(ReferencesBook rbk){
		blist.add(rbk);
	}
	
	public void borrowedBook(Student stu,String bk){
		
		for(int i=0; i<blist.size()-1; i++){
			if(blist.get(i).getBname().equals(bk)){
				Book b=blist.remove(i);
				stu.addbook(b);
			}
		}
	}
	
	public void returnedBook(Student stu,String bk){
		for(int i=0; i<stu.getBooks().size()-1; i++){
			
			if(stu.getBooks().get(i).getBname().equals(bk)){
				Book b=stu.getBooks().remove(i);
				blist.add(b);
			}
		}
	}
	
	
	public void iborrowedBook(Instructor in,String rbk){
		
		for(int i=0; i<blist.size()-1; i++){
			if(blist.get(i).getBname().equals(rbk)){
				Book b=blist.remove(i);
				in.addbook(b);
			}
		}
	}
	
	public void ireturnedBook(Instructor in,String rbk){
		for(int i=0; i<in.getBooks().size()-1; i++){
			
			if(in.getBooks().get(i).getBname().equals(rbk)){
				Book b=in.getBooks().remove(i);
				blist.add(b);
			}
		}
	}
	
	public int bookCount(){
		return blist.size();
	}

	public String getBorrower(){
		return "";
		
	}
}
