package library;

public class Main {

	public static void main(String[] args) {
		
		Library lib = new Library();

		Student stu = new Student("Orapun","Numchokchaijarernkul","5610404711");
		Student stu2 = new Student("Sirinaree","Hinjiranandana","5610404631");
		Instructor in = new Instructor("Usa","Summapan","D14");
		Staff st = new Staff("Gaew","Lastname");
		
		Book bk = new Book("Big JAVA",400,"Watson");
		ReferencesBook rbk = new ReferencesBook("OOP",300,"Cay");
		
		
		lib.addBook(bk);
		lib.addRefBook(rbk);
		lib.borrowedBook(stu,"Big JAVA");
		lib.iborrowedBook(in,"OOP");
		
		System.out.println(lib.bookCount());
		System.out.println(stu.getBorrowedBook());
		System.out.println(in.getBorrowedBook());
		System.out.println(lib.getBorrower());
		
		System.out.println(stu.getName()+" "+stu.getLastname()+", "+stu.getId());
		System.out.println(stu2.getName()+" "+stu2.getLastname()+", "+stu2.getId());
		System.out.println(lib.bookCount());
		
		System.out.println(in.getName()+" "+in.getLastname()+", "+in.getMajor());	
		System.out.println(st.getName()+" "+st.getLastname());
		System.out.println(bk.getBname()+", "+bk.getNumber()+", "+bk.getAuthor());
		System.out.println(rbk.getBname()+", "+rbk.getNumber()+", "+rbk.getAuthor());
		
		
	}
	
}
